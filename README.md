#DOCUMENTACIÓN API COVID-19-ES

Documentación definida en el formato [openapi 3.0](https://swagger.io/docs/specification/about/) usando [Create React App](https://github.com/facebook/create-react-app) y [ReDoc](https://github.com/Redocly/redoc) para generar el explorador de la API para el proyecto interno de Secuoyas de analísis e investigación sobre coronavirus en España.
