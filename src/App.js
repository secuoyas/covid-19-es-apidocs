import React from 'react';
import { RedocStandalone } from 'redoc';
import openApi from './openapi.js';
function App() {
	return <RedocStandalone spec={openApi} />;
}

export default App;
