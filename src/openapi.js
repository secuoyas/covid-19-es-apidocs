const data = {
	openapi: '3.0.0',
	info: {
		title: 'COVID19 ES API',
		description:
			'# Descripción\nPuedes consultar todos los datos sobre el covid-19 de España, a nivel pais, comunidad autonóma y provincia obtenidos de fuentes oficiales desde un mismo sitio.',
		contact: {
			email: 'hola@secuoyas.com',
			name: 'Secuoyas',
			url: 'http://secuoyas.com',
		},
		license: {
			name: 'Apache 2.0',
			url: 'http://www.apache.org/licenses/LICENSE-2.0.html',
		},
		version: '0.4',
	},
	servers: [
		{
			url: 'https://covid19.secuoyas.com/api/v1',
			description: 'COVID-19-es API',
		},
	],
	tags: [
		{
			name: 'España',
			description:
				'Puedes consultar todos los datos de España obtenidos de fuentes oficiales desde un mismo sitio.\nLos datos están generados a partir del acumulado de las comunidades autónomas y estan disponibles agrupando todos los dias o eligiendo un dia en concreto, tambien está disponible la opción de consultar el último registo.',
		},
		{
			name: 'Comunidades Autónomas',
			description:
				'Puedes consultar los datos de todas las comunidades autónomas obtenidos de fuentes oficiales desde un mismo sitio.\nLos datos estan disponibles agrupando todos los dias o eligiendo un dia en concreto, tambien está disponible la opción de consultar el último registo, tanto para una comunidad aútonoma concreta o para todas a la vez.',
		},
		{
			name: 'Provincias',
			description:
				'Puedes consultar los datos de todas las provincias de las que hemos obtenido datos de fuentes oficiales desde un mismo sitio.\n\nLos datos estan disponibles agrupando todos los dias o eligiendo un dia en concreto, tambien está disponible la opción de consultar el último registo, tanto para una provincia concreta o para todas las provincias a la vez. ',
		},
	],
	paths: {
		'/es': {
			get: {
				tags: ['España'],
				description:
					'Para acceder a los recursos estan disponibles estas direcciones:\n\n### Datos de todos los dias agrupados\n\n```https://covid19.secuoyas.com/api/v1/es```\n\n\n### Datos de un solo dia\n\n```https://covid19.secuoyas.com/api/v1/es?fecha=2020-04-01```\n\nfecha: La fecha debe ser escrita en el formato YYYY-MM-DD es decir primero el año expresado con 4 digitos, luego el mes mostrado siempre con dos dígitos y el dia con dos digitos, separados por un guión -.\n\n\n### Últimos datos diarios\n\n```https://covid19.secuoyas.com/api/v1/es/ccaa?ultimodia=true```\n\nnota: si se añaden ultimo dia y fecha como parámetros se tomará ultimodia=true\n\n ',

				responses: {
					'200': {
						description: 'OK',
						content: {
							'application/json': {
								schema: {
									type: 'array',
									items: {
										type: 'object',
										properties: {
											trace: {
												type: 'object',
												properties: {
													info: {
														type: 'object',
														properties: {
															nombre: {
																type: 'string',
															},
															creado: {
																type: 'string',
															},
															fecha: {
																type: 'string',
															},
															fechaInicio: {
																type: 'string',
															},
															fechaFin: {
																type: 'string',
															},
															numeroFechas: {
																type: 'number',
															},
															periodo: {
																type: 'string',
															},
															nivelAdministrativo: {
																type: 'string',
															},
															nombreLugar: {
																type: 'string',
															},
															codigoIsoLugar: {
																type: 'string',
															},
															numeroLugares: {
																type: 'number',
															},
														},
													},
													contacto: {
														type: 'object',
														properties: {
															nombreEmpresa: {
																type: 'string',
																example: 'Secuoyas',
															},
															url: {
																type: 'string',
																example: 'http://secuoyas.com',
															},
															email: {
																type: 'string',
																example: 'hola@secuoyas.com',
															},
														},
													},
												},
											},
											timeline: {
												type: 'array',
												items: {
													type: 'object',
													properties: {
														nombre: {
															type: 'string',
														},
														fecha: {
															type: 'string',
														},
														fechaInicio: {
															type: 'string',
														},
														fechFin: {
															type: 'string',
														},
														numeroFechas: {
															type: 'number',
														},
														periodo: {
															type: 'string',
														},
														regiones: {
															type: 'array',
															items: {
																type: 'object',
																properties: {
																	nombre: {
																		type: 'string',
																	},
																	nivelAdministrativo: {
																		type: 'string',
																	},
																	nombreLugar: {
																		type: 'string',
																	},
																	codigoIsoLugar: {
																		type: 'string',
																	},
																	long: {
																		type: 'number',
																	},
																	lat: {
																		type: 'number',
																	},
																	data: {
																		type: 'object',
																		properties: {
																			casosConfirmados: {
																				type: 'number',
																			},
																			casosUci: {
																				type: 'number',
																			},
																			casosFallecidos: {
																				type: 'number',
																			},
																			casosHospitalizados: {
																				type: 'number',
																			},
																			casosRecuperados: {
																				type: 'number',
																			},
																			casosConfirmadosDiario: {
																				type: 'number',
																			},
																			casosUciDiario: {
																				type: 'number',
																			},
																			casosFallecidosDiario: {
																				type: 'number',
																			},
																		},
																	},
																},
															},
														},
													},
												},
											},
										},
									},
								},
							},
						},
						headers: {},
					},
				},
				parameters: [
					{
						schema: {
							type: 'string',
						},
						in: 'query',
						name: 'fecha',
						description: 'formato YYYY-MM-DD',
					},
					{
						schema: {
							type: 'boolean',
						},
						in: 'query',
						name: 'ultimodia',
						description: 'ultimodia=true *sobreescribe la fecha',
					},
				],
				operationId: '/es',
			},
		},
		'/es/ccaa': {
			get: {
				tags: ['Comunidades Autónomas'],
				description:
					'\n\nPara acceder a los recursos estan disponibles estas direcciones:\n\n### Datos de todos los dias para todas las CCAA\n\n```https://covid19.secuoyas.com/api/v1/es/ccaa```\n\n### Datos de todos los dias de una CCAA  \n\n```https://covid19.secuoyas.com/api/v1/es/ccaa?codigo=es-an```\n\ncodigo: Es el código Iso 3166:2:ES, en este caso "es-an" es el código Iso para la comunidad autonoma andaluza. Ver el resto de códigos.\n\n### Datos de un solo dia para todas las CCAA \n\n```https://covid19.secuoyas.com/api/v1/es/ccaa?fecha=2020-04-01```\n\nfecha: La fecha debe ser escrita en el formato YYYY-MM-DD es decir primero el año expresado con 4 digitos, luego el mes mostrado siempre con dos dígitos y el dia con dos digitos, separados por un guión -.\n\n### Datos de un solo dia para una CCAA \n\n```https://covid19.secuoyas.com/api/v1/es/ccaa?fecha=2020-04-01&codigo=es-an```\n\ncodigo: Es el código Iso 3166:2:ES, en este caso "es-an" es el código Iso para la comunidad autonoma andaluza. Ver el resto de códigos.\nfecha: La fecha debe ser escrita en el formato YYYY-MM-DD es decir primero el año expresado con 4 digitos, luego el mes mostrado siempre con dos dígitos y el dia con dos digitos, separados por un guión -.\n\n### Últimos datos de todas las CCAA\n\n```https://covid19.secuoyas.com/api/v1/es/ccaa?ultimodia=true```\n\nnota: si se añaden ultimo dia y fecha como parámetros se tomará ultimodia=true\n\n### Últimos datos de una CCAA\n\n```https://covid19.secuoyas.com/api/v1/es/ccaa??ultimodia=true&codigo=es-an```\n\nnota: si se añaden ultimo dia y fecha como parámetros se tomará ultimodia=true\ncodigo: Es el código Iso 3166:2:ES, en este caso "es-an" es el código Iso para la comunidad autonoma andaluza. Ver el resto de códigos.\n\n',
				parameters: [
					{
						in: 'query',
						name: 'fecha',
						required: false,
						description: 'formato YYYY-MM-DD',
						schema: {
							type: 'string',
						},
					},
					{
						in: 'query',
						name: 'ultimodia',
						required: false,
						description: 'ultimodia=true *sobreescribe la fecha',
						schema: {
							type: 'string',
						},
					},
					{
						in: 'query',
						name: 'codigo',
						required: false,
						description: 'Código Iso 3166:2:ES',
						schema: {
							type: 'string',
							enum: [
								'ES-AN',
								'ES-AR',
								'ES-AS',
								'ES-CN',
								'ES-CB',
								'ES-CM',
								'ES-CL',
								'ES-CT',
								'ES-EX',
								'ES-GA',
								'ES-IB',
								'ES-RI',
								'ES-MD',
								'ES-MC',
								'ES-NC',
								'ES-PV',
								'ES-VC',
								'ES-CE',
								'ES-ME',
							],
						},
					},
				],
				responses: {
					'200': {
						description: 'OK',
						content: {
							'application/json': {
								schema: {
									type: 'array',
									items: {
										type: 'object',
										properties: {
											trace: {
												type: 'object',
												properties: {
													info: {
														type: 'object',
														properties: {
															nombre: {
																type: 'string',
															},
															creado: {
																type: 'string',
															},
															fecha: {
																type: 'string',
															},
															fechaInicio: {
																type: 'string',
															},
															fechaFin: {
																type: 'string',
															},
															numeroFechas: {
																type: 'number',
															},
															periodo: {
																type: 'string',
															},
															nivelAdministrativo: {
																type: 'string',
															},
															nombreLugar: {
																type: 'string',
															},
															codigoIsoLugar: {
																type: 'string',
															},
															numeroLugares: {
																type: 'number',
															},
														},
													},
													contacto: {
														type: 'object',
														properties: {
															nombreEmpresa: {
																type: 'string',
																example: 'Secuoyas',
															},
															url: {
																type: 'string',
																example: 'http://secuoyas.com',
															},
															email: {
																type: 'string',
																example: 'hola@secuoyas.com',
															},
														},
													},
												},
											},
											timeline: {
												type: 'array',
												items: {
													type: 'object',
													properties: {
														nombre: {
															type: 'string',
														},
														fecha: {
															type: 'string',
														},
														fechaInicio: {
															type: 'string',
														},
														fechFin: {
															type: 'string',
														},
														numeroFechas: {
															type: 'number',
														},
														periodo: {
															type: 'string',
														},
														regiones: {
															type: 'array',
															items: {
																type: 'object',
																properties: {
																	nombre: {
																		type: 'string',
																	},
																	nivelAdministrativo: {
																		type: 'string',
																	},
																	nombreLugar: {
																		type: 'string',
																	},
																	codigoIsoLugar: {
																		type: 'string',
																	},
																	long: {
																		type: 'number',
																	},
																	lat: {
																		type: 'number',
																	},
																	data: {
																		type: 'object',
																		properties: {
																			casosConfirmados: {
																				type: 'number',
																			},
																			casosUci: {
																				type: 'number',
																			},
																			casosFallecidos: {
																				type: 'number',
																			},
																			casosHospitalizados: {
																				type: 'number',
																			},
																			casosRecuperados: {
																				type: 'number',
																			},
																			casosConfirmadosDiario: {
																				type: 'number',
																			},
																			casosUciDiario: {
																				type: 'number',
																			},
																			casosFallecidosDiario: {
																				type: 'number',
																			},
																		},
																	},
																},
															},
														},
													},
												},
											},
										},
									},
								},
							},
						},
					},
				},
				operationId: '/es/ccaa',
			},
		},
		'/es/ccaa/prov': {
			get: {
				description:
					'Para acceder a los recursos estan disponibles estas direcciones:\n\n### Datos de todos los dias para todas las provincias disponibles\n\n```https://covid19.secuoyas.com/api/v1/es/ccaa/prov```\n\n### Datos de todos los dias de una provincia \n\n```https://covid19.secuoyas.com/api/v1/es/ccaa/prov?codigo=es-al```\n\ncodigo: Es el código Iso 3166:2:ES, en este caso "es-al" es el código Iso para la provincia almeriense. Ver el resto de códigos.\n\n### Datos de un solo dia para todas las provincias disponibles \n\n```https://covid19.secuoyas.com/api/v1/es/ccaa/prov?fecha=2020-04-01```\n\nfecha: La fecha debe ser escrita en el formato YYYY-MM-DD es decir primero el año expresado con 4 digitos, luego el mes mostrado siempre con dos dígitos y el dia con dos digitos, separados por un guión -.\n\n### Datos de un solo dia para una provincia \n\n```https://covid19.secuoyas.com/api/v1/es/ccaa/prov?fecha=2020-04-01&codigo=es-an```\n\ncodigo: Es el código Iso 3166:2:ES, en este caso "es-al" es el código Iso para la provincia almeriense. Ver el resto de códigos.\nfecha: La fecha debe ser escrita en el formato YYYY-MM-DD es decir primero el año expresado con 4 digitos, luego el mes mostrado siempre con dos dígitos y el dia con dos digitos, separados por un guión -.\n\n### Últimos datos de todas las provincias disponibles\n\n```https://covid19.secuoyas.com/api/v1/es/ccaa/prov?ultimodia=true```\n\nnota: si se añaden ultimo dia y fecha como parámetros se tomará ultimodia=true\n\n### Últimos datos de una provincia\n```https://covid19.secuoyas.com/api/v1/es/ccaa/prov??ultimodia=true&codigo=es-an```\n\nnota: si se añaden ultimo dia y fecha como parámetros se tomará ultimodia=true\ncodigo: Es el código Iso 3166:2:ES, en este caso "es-al" es el código Iso para la provincia almeriense. Ver el resto de códigos.',
				tags: ['Provincias'],

				operationId: '/es/ccaa/prov',
				parameters: [
					{
						in: 'query',
						name: 'fecha',
						required: false,
						description: 'formato YYYY-MM-DD',
						schema: {
							type: 'string',
						},
					},
					{
						in: 'query',
						name: 'ultimodia',
						required: false,
						description: 'ultimodia=true *sobreescribe la fecha',
						schema: {
							type: 'string',
						},
					},
					{
						in: 'query',
						name: 'codigo',
						required: false,
						description: 'Código Iso 3166:2:ES',
						schema: {
							type: 'string',
							enum: [
								'ES-C',
								'ES-VI',
								'ES-AB',
								'ES-A',
								'ES-AL',
								'ES-O',
								'ES-AV',
								'ES-BA',
								'ES-PM',
								'ES-B ',
								'ES-BI',
								'ES-BU',
								'ES-CC',
								'ES-CA',
								'ES-S',
								'ES-CS',
								'ES-CR',
								'ES-CO',
								'ES-CU',
								'ES-SS',
								'ES-GI',
								'ES-GR',
								'ES-GU',
								'ES-H ',
								'ES-HU',
								'ES-J ',
								'ES-LO',
								'ES-GC',
								'ES-LE',
								'ES-L ',
								'ES-LU',
								'ES-M ',
								'ES-MA',
								'ES-MU',
								'ES-NA',
								'ES-OR',
								'ES-P ',
								'ES-PO',
								'ES-SA',
								'ES-TF',
								'ES-SG',
								'ES-SE',
								'ES-SO',
								'ES-T',
								'ES-T',
								'ES-T',
								'ES-V',
								'ES-V',
								'ES-Z',
								'ES-Z',
							],
						},
					},
				],
				responses: {
					'200': {
						description: 'OK',
						content: {
							'application/json': {
								schema: {
									type: 'array',
									items: {
										type: 'object',
										properties: {
											trace: {
												type: 'object',
												properties: {
													info: {
														type: 'object',
														properties: {
															nombre: {
																type: 'string',
															},
															creado: {
																type: 'string',
															},
															fecha: {
																type: 'string',
															},
															fechaInicio: {
																type: 'string',
															},
															fechaFin: {
																type: 'string',
															},
															numeroFechas: {
																type: 'number',
															},
															periodo: {
																type: 'string',
															},
															nivelAdministrativo: {
																type: 'string',
															},
															nombreLugar: {
																type: 'string',
															},
															codigoIsoLugar: {
																type: 'string',
															},
															numeroLugares: {
																type: 'number',
															},
														},
													},
													contacto: {
														type: 'object',
														properties: {
															nombreEmpresa: {
																type: 'string',
																example: 'Secuoyas',
															},
															url: {
																type: 'string',
																example: 'http://secuoyas.com',
															},
															email: {
																type: 'string',
																example: 'hola@secuoyas.com',
															},
														},
													},
												},
											},
											timeline: {
												type: 'array',
												items: {
													type: 'object',
													properties: {
														nombre: {
															type: 'string',
														},
														fecha: {
															type: 'string',
														},
														fechaInicio: {
															type: 'string',
														},
														fechFin: {
															type: 'string',
														},
														numeroFechas: {
															type: 'number',
														},
														periodo: {
															type: 'string',
														},
														regiones: {
															type: 'array',
															items: {
																type: 'object',
																properties: {
																	nombre: {
																		type: 'string',
																	},
																	nivelAdministrativo: {
																		type: 'string',
																	},
																	nombreLugar: {
																		type: 'string',
																	},
																	codigoIsoLugar: {
																		type: 'string',
																	},
																	long: {
																		type: 'number',
																	},
																	lat: {
																		type: 'number',
																	},
																	data: {
																		type: 'object',
																		properties: {
																			casosConfirmados: {
																				type: 'number',
																			},
																			casosUci: {
																				type: 'number',
																			},
																			casosFallecidos: {
																				type: 'number',
																			},
																			casosHospitalizados: {
																				type: 'number',
																			},
																			casosRecuperados: {
																				type: 'number',
																			},
																			casosConfirmadosDiario: {
																				type: 'number',
																			},
																			casosUciDiario: {
																				type: 'number',
																			},
																			casosFallecidosDiario: {
																				type: 'number',
																			},
																		},
																	},
																},
															},
														},
													},
												},
											},
										},
									},
								},
							},
						},
					},
				},
			},
		},
	},
	components: {
		schemas: {
			Casos: {
				type: 'object',
				properties: {
					trace: {
						type: 'object',
						properties: {
							info: {
								type: 'object',
								properties: {
									nombre: {
										type: 'string',
									},
									creado: {
										type: 'string',
									},
									fecha: {
										type: 'string',
									},
									fechaInicio: {
										type: 'string',
									},
									fechaFin: {
										type: 'string',
									},
									numeroFechas: {
										type: 'number',
									},
									periodo: {
										type: 'string',
									},
									nivelAdministrativo: {
										type: 'string',
									},
									nombreLugar: {
										type: 'string',
									},
									codigoIsoLugar: {
										type: 'string',
									},
									numeroLugares: {
										type: 'number',
									},
								},
							},
							contacto: {
								type: 'object',
								properties: {
									nombreEmpresa: {
										type: 'string',
										example: 'Secuoyas',
									},
									url: {
										type: 'string',
										example: 'http://secuoyas.com',
									},
									email: {
										type: 'string',
										example: 'hola@secuoyas.com',
									},
								},
							},
						},
					},
					timeline: {
						type: 'array',
						items: {
							type: 'object',
							properties: {
								nombre: {
									type: 'string',
								},
								fecha: {
									type: 'string',
								},
								fechaInicio: {
									type: 'string',
								},
								fechFin: {
									type: 'string',
								},
								numeroFechas: {
									type: 'number',
								},
								periodo: {
									type: 'string',
								},
								regiones: {
									type: 'array',
									items: {
										type: 'object',
										properties: {
											nombre: {
												type: 'string',
											},
											nivelAdministrativo: {
												type: 'string',
											},
											nombreLugar: {
												type: 'string',
											},
											codigoIsoLugar: {
												type: 'string',
											},
											long: {
												type: 'number',
											},
											lat: {
												type: 'number',
											},
											data: {
												type: 'object',
												properties: {
													casosConfirmados: {
														type: 'number',
													},
													casosUci: {
														type: 'number',
													},
													casosFallecidos: {
														type: 'number',
													},
													casosHospitalizados: {
														type: 'number',
													},
													casosRecuperados: {
														type: 'number',
													},
													casosConfirmadosDiario: {
														type: 'number',
													},
													casosUciDiario: {
														type: 'number',
													},
													casosFallecidosDiario: {
														type: 'number',
													},
												},
											},
										},
									},
								},
							},
						},
					},
				},
			},
			Info: {
				type: 'object',
				properties: {
					nombre: {
						type: 'string',
					},
					creado: {
						type: 'string',
					},
					fecha: {
						type: 'string',
					},
					fechaInicio: {
						type: 'string',
					},
					fechaFin: {
						type: 'string',
					},
					numeroFechas: {
						type: 'number',
					},
					periodo: {
						type: 'string',
					},
					nivelAdministrativo: {
						type: 'string',
					},
					nombreLugar: {
						type: 'string',
					},
					codigoIsoLugar: {
						type: 'string',
					},
					numeroLugares: {
						type: 'number',
					},
				},
			},
			Contacto: {
				type: 'object',
				properties: {
					nombreEmpresa: {
						type: 'string',
						example: 'Secuoyas',
					},
					url: {
						type: 'string',
						example: 'http://secuoyas.com',
					},
					email: {
						type: 'string',
						example: 'hola@secuoyas.com',
					},
				},
			},
			Date: {
				type: 'object',
				properties: {
					nombre: {
						type: 'string',
					},
					fecha: {
						type: 'string',
					},
					fechaInicio: {
						type: 'string',
					},
					fechFin: {
						type: 'string',
					},
					numeroFechas: {
						type: 'number',
					},
					periodo: {
						type: 'string',
					},
					regiones: {
						type: 'array',
						items: {
							type: 'object',
							properties: {
								nombre: {
									type: 'string',
								},
								nivelAdministrativo: {
									type: 'string',
								},
								nombreLugar: {
									type: 'string',
								},
								codigoIsoLugar: {
									type: 'string',
								},
								long: {
									type: 'number',
								},
								lat: {
									type: 'number',
								},
								data: {
									type: 'object',
									properties: {
										casosConfirmados: {
											type: 'number',
										},
										casosUci: {
											type: 'number',
										},
										casosFallecidos: {
											type: 'number',
										},
										casosHospitalizados: {
											type: 'number',
										},
										casosRecuperados: {
											type: 'number',
										},
										casosConfirmadosDiario: {
											type: 'number',
										},
										casosUciDiario: {
											type: 'number',
										},
										casosFallecidosDiario: {
											type: 'number',
										},
									},
								},
							},
						},
					},
				},
			},
			Region: {
				type: 'object',
				properties: {
					nombre: {
						type: 'string',
					},
					nivelAdministrativo: {
						type: 'string',
					},
					nombreLugar: {
						type: 'string',
					},
					codigoIsoLugar: {
						type: 'string',
					},
					long: {
						type: 'number',
					},
					lat: {
						type: 'number',
					},
					data: {
						type: 'object',
						properties: {
							casosConfirmados: {
								type: 'number',
							},
							casosUci: {
								type: 'number',
							},
							casosFallecidos: {
								type: 'number',
							},
							casosHospitalizados: {
								type: 'number',
							},
							casosRecuperados: {
								type: 'number',
							},
							casosConfirmadosDiario: {
								type: 'number',
							},
							casosUciDiario: {
								type: 'number',
							},
							casosFallecidosDiario: {
								type: 'number',
							},
						},
					},
				},
			},
			CasosData: {
				type: 'object',
				properties: {
					casosConfirmados: {
						type: 'number',
					},
					casosUci: {
						type: 'number',
					},
					casosFallecidos: {
						type: 'number',
					},
					casosHospitalizados: {
						type: 'number',
					},
					casosRecuperados: {
						type: 'number',
					},
					casosConfirmadosDiario: {
						type: 'number',
					},
					casosUciDiario: {
						type: 'number',
					},
					casosFallecidosDiario: {
						type: 'number',
					},
				},
			},
		},
		securitySchemes: {},
	},
};

export default data;
